<?php

namespace Drupal\ckeditor_custom_paste_filters\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "custompastefilters" plugin.
 *
 * @CKEditorPlugin(
 *   id = "custompastefilters",
 *   label = @Translation("CKEditor Custom Paste Filters"),
 *   module = "ckeditor_custom_paste_filters"
 * )
 */
class CustomPasteFilters extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface, CKEditorPluginContextualInterface {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $modulePath = drupal_get_path('module', 'ckeditor_custom_paste_filters');
    $pluginPath = '/js/plugins/custom_paste_filters/plugin.js';

    return $modulePath . $pluginPath;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(
    array $form,
    FormStateInterface $form_state,
    Editor $editor
  ) {
    $settings = $editor->getSettings();
    $field_values = [];

    if (isset($settings['plugins']['custompastefilters'])) {
      $field_values = $settings['plugins']['custompastefilters'];
    }

    $form['heading'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('CKEditor Custom Paste Filters'),
      '#attached' => [
        'library' => ['ckeditor_custom_paste_filters/config_summary'],
      ],
    ];

    $form['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('
        This plugin implements some very specific paste filters
        for the CKEditor.<br>Check the options below to activate
        the desired filters.'
      ),
    ];

    $form['divider_01'] = [
      '#type' => 'html_tag',
      '#tag' => 'hr',
    ];

    $form['nbsp_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Removes <strong>&amp;nbsp;</strong> on paste'),
      '#default_value' => (
        isset($field_values['nbsp_filter'])
        ? $field_values['nbsp_filter']
        : ''
      ),
      '#description' => $this->t('
        Remove all occurrences of <strong>&amp;nbsp;</strong> from the text
        during the <strong>PASTE</strong> process.'
      ),
    ];

    $form['divider_02'] = [
      '#type' => 'html_tag',
      '#tag' => 'hr',
    ];

    $form['empty_paragraph_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Removes <strong>empty paragraphs</strong> on paste'),
      '#default_value' => (
        isset($field_values['empty_paragraph_filter'])
        ? $field_values['empty_paragraph_filter']
        : ''
      ),
      '#description' => $this->t('
        Remove all occurrences of <strong>empty paragraphs</strong>
        or paragraphs that have only <strong>spaces inside</strong>
        during the <strong>PASTE</strong> process.'
      ),
    ];

    $form['divider_03'] = [
      '#type' => 'html_tag',
      '#tag' => 'hr',
    ];

    $form['paragraph_inside_list_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Removes <strong>paragraphs inside lists</strong> on paste'),
      '#default_value' => (
        isset($field_values['paragraph_inside_list_filter'])
        ? $field_values['paragraph_inside_list_filter']
        : ''
      ),
      '#description' => $this->t('
        Remove all occurrences of <strong>paragraphs inside lists</strong>
        during the <strong>PASTE</strong> process.'
      ),
    ];

    return $form;
  }

}
