(function ($, Drupal) {
  Drupal.behaviors.ckeditorCustomPasteFiltersConfigSummary = {
    attach: function attach() {
      var tabContainer = $('[data-ckeditor-plugin-id="custompastefilters"]');

      tabContainer.drupalSetSummary(function (context) {
        var selectedFilters = $(context).find('input[type="checkbox"]:checked');
        var selectedFiltersCounter = selectedFilters.length;

        if (selectedFiltersCounter > 1) {
          return Drupal.t(selectedFiltersCounter + ' filters active');
        } else if (selectedFiltersCounter > 0) {
          return Drupal.t('1 filter active');
        } else {
          return Drupal.t('No filter active');
        }
      });
    }
  };
})(jQuery, Drupal);
