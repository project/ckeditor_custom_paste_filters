/**
 * @file
 * Custom Paste Filters CKEditor plugin.
 *
 * Plugin to add some specific paste filters do CKEditor.
 */

 (function () {
  'use strict';

  // Function to get all nodes from received data
  function getDataElements(data) {
    let container = document.createElement('div');
    container.insertAdjacentHTML('beforeend', data);
    return container.childNodes;
  }

  // Function to apply the filters
  let apply_filter = {
    nbsp : function(data) {
      return data.replace(/\s/g,' ');
    },
    empty_paragraph : function(data) {
      const elements = getDataElements(data);
      let html = '';

      elements.forEach(element => {
        let elementHtml = element.outerHTML;

        if (element.nodeName === 'P' && element.innerText.trim() === '') {
          return;
        }

        if (element.nodeName === '#text' && element.nodeValue.trim() !== '') {
          elementHtml = element.nodeValue;
        }

        html += elementHtml;
      });

      return html;
    },
    paragraph_inside_list : function(data) {
      const elements = getDataElements(data);
      let html = '';

      elements.forEach(element => {
        let elementHtml = element.outerHTML;

        if (element.nodeName === '#text' && element.nodeValue.trim() !== '') {
          elementHtml = element.nodeValue;
        }

        if (element.nodeName === 'UL' || element.nodeName === 'OL') {
          elementHtml = elementHtml
            .replaceAll('</p><p>', '<br/>')
            .replaceAll('<p>', '')
            .replaceAll('</p>', '');
        }

        html += elementHtml;
      });

      return html;
    }
  };

  // Add the Custom Paste Filters plugin to CKEditor
  CKEDITOR.plugins.add('custompastefilters', {
    init : function( editor ) {
      var filterSettings = editor.config.customPasteFilters;

      if (filterSettings) {
        var nbsp_filter = filterSettings.nbsp_filter;
        var empty_paragraph_filter = filterSettings.empty_paragraph_filter;
        var paragraph_inside_list_filter = filterSettings.paragraph_inside_list_filter;

        editor.on( 'paste', function(evt) {
          var ckeditor_data = evt.data.dataValue;

          if (nbsp_filter) {
            ckeditor_data = apply_filter.nbsp(ckeditor_data);
          }

          if (empty_paragraph_filter) {
            ckeditor_data = apply_filter.empty_paragraph(ckeditor_data);
          }

          if (paragraph_inside_list_filter) {
            ckeditor_data = apply_filter.paragraph_inside_list(ckeditor_data);
          }

          evt.data.dataValue = ckeditor_data;
        });
      }
    }
  });
} ());
