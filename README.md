CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module complements the "pasteFilter" CKEditor configuration. It does not
extends the "pasteFilter" configuration, because this module works as stand
alone by implementing a CKEditor plugin that adds some very specific filters
on de paste process.

For now this module only implements the removal of the `&nbsp;`, empty
paragraphs and paragraphs inside lists from the text during the paste process.
But in the future we can add more filters if needed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/ckeditor_custom_paste_filters

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/ckeditor_custom_paste_filters


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------

 * [CKEditor module](https://www.drupal.org/docs/8/core/modules/ckeditor):
   WYSIWYG editing for rich text fields using CKEditor.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Go to Administration » Configuration » Content authoring:

  - Select any of the CKEditor and click on Configure.

  - In the CKEditor plugin settings section, click on the CKEditor Custom
    Paste Filters tab and enable the desired filters and save it.

  - Now after pasting a content from word or other places the filter will be
    applied to the selected CKEditor.

To filter tags on the paste process, we suggest install the module CKEditor
custom config (https://www.drupal.org/project/ckeditor_config) and add
pasteFilter to the configuration, selecting only the valid tags that will be
accepted during the paste process.

We did not use Limit allowed HTML tags and correct faulty HTML filter because
some times we have custom classes added to the CKEditor and this filter if not
correct configured could cause some collateral effects.

But in most cases, this is a really good option to be followed.


MAINTAINERS
-----------

Current maintainers:
 * Giancarlo Rosa (giancarlorosa) - https://www.drupal.org/u/giancarlorosa
